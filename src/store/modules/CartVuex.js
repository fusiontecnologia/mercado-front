// import Api from '../../api/index'

const Cart = {
  namespaced: true,
  state: {
    allCarts: null, // [],
    
    oneCart: {
      id: 1,
      owner: 'fasdf',
      items: []
    }, // {}

    success: null,
    success_msg: null,
    error: null,
    error_msg: null,
  },
  getters: {
    getAllCarts: state => {
      return state.allCarts;
    },
    getOneCartItems: state => {
      return state.oneCart.items;
    }
  },

  mutations: {
    setOneItem(state, item){
      state.oneCart = item // item.data
    },
    addItem(state, item){
      state.oneCart.items.push(item)
    },

  },

  actions: {

    addToCart(context, item){
      // chama a mutation para colocar no state o item
      context.commit('addItem', item)
    },

    index(context, filter){
      let Carts_no_banco_de_dados = [
        {name: 'Queijo',  value: 10},
        {name: 'Bacon',   value: 10},
        {name: 'Batata',  value: 10},
        {name: 'Maçã',    value: 10},
      ]
      setTimeout(() => {
        context.commit('setAllItems', Carts_no_banco_de_dados)
      }, 2000);
    },
    show(){},
    create(){},
    update(){},
    destroy(){},
    

    indexTEST(context, filter) {
      Api.Item.index(filter)
        .then(response => response.data)
        .then(items => {
          context.commit('setAllItems', items);
          context.commit('indexResponse', {msg: '', was_good: true});
        }).catch(function(error) {
          context.commit('indexResponse', {msg: error.response.data.errors[0], was_good: false});
          console.log(error);
        });
    },
  }

};

export default Cart;