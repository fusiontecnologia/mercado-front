// import Api from '../../api/index'

const Product = {
  namespaced: true,
  state: {
    allProducts: null, // [],
    
    newProducts: null, // [],

    oneProduct: null, // {}

    success: null,
    success_msg: null,
    error: null,
    error_msg: null,
  },
  getters: {
    getAllProducts: state => {
      return state.allProducts;
    }
  },

  mutations: {
    setOneItem(state, item){
      state.oneProduct = item // item.data
    },
    setAllItems(state, items){
      state.allProducts = items // items.data
    },
  },

  actions: {

    // index, show, create, update, destroy/delete

      // index => listar todos do mesmo do MODEL
      // show => listar apenas 1 do model 
      // create => criar um novo produto, ou entidade do model
      // update => atualizar uma entidade do model
    // destroy/delete => deletar


    index(context, filter){
      let products_no_banco_de_dados = [
        {name: 'Queijo',  value: 10},
        {name: 'Bacon',   value: 10},
        {name: 'Batata',  value: 10},
        {name: 'Maçã',    value: 10},
      ]
      setTimeout(() => {
        context.commit('setAllItems', products_no_banco_de_dados)
      }, 2000);
    },
    show(){},
    create(){},
    update(){},
    destroy(){},
    

    indexTEST(context, filter) {
      Api.Item.index(filter)
        .then(response => response.data)
        .then(items => {
          context.commit('setAllItems', items);
          context.commit('indexResponse', {msg: '', was_good: true});
        }).catch(function(error) {
          context.commit('indexResponse', {msg: error.response.data.errors[0], was_good: false});
          console.log(error);
        });
    },
  }

};

export default Product;